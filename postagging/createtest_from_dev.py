__author__ = 'mohit'
import sys

def main():

    output_file = sys.argv[1]

    with open(output_file, 'w') as output_hdl:
        for line in sys.stdin.readlines():


            line = line.strip()

            words = line.split()

            newline = ''
            delim = ''
            for word in words:

                word_label = word.split('/')
                output_hdl.write(word_label[1]+'\n')
                newline += delim + word_label[0]
                delim = ' '
            print(newline)

if __name__ == "__main__":
    main()