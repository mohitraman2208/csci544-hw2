__author__ = 'mohit'
import sys
import os.path
from perceptron import Perceptron
import json

def get_labels(train_file_name):
    labels = []
    with open(train_file_name, 'r', errors='ignore') as train_file:
        for line in train_file:
            label = line.split()[0]
            if label not in labels:
                labels.append(label)
    return labels

def main():
    # This is the training program for percepclassify Perceptron Classifier
    # Take two command line arguments - training_file model_file
    # training_file contains training data and the learned model is stored in model file

    if len(sys.argv) != 3 or sys.argv[1] == '' or not os.path.isfile(os.path.abspath(sys.argv[1])) or sys.argv[2] == '':
        print("Invalid Arguments!")
        exit(1)

    # Command line argument 1 - Training file name
    train_file_name = sys.argv[1]

    # Command line argument 2 - Model file name
    model_file_name = sys.argv[2]

    labels = get_labels(train_file_name)

    percepLearn = Perceptron()
    percepLearn.init_percep_learn(train_file_name, model_file_name, labels, 25)

    model = percepLearn.percep_learn()

    with open(model_file_name,'w') as output_file:
        output_file.write(json.dumps(model,indent=1))


if __name__ == "__main__":
    main()
