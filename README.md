# README #

Homework 2 - Perceptron, part-of-speech tagging and named entity recognition
### Instructions to run the perceptron ###

Creating model file from training data - 

python3.4 perceplearn.py training_file_name model_file_name

Classifying Test data

python3.4 percepclassify.py model_file_name < test_file_name > output_file_name

Note: poslearn and nelearn creates a new intermediate file( not a tempfile) which is deleted just before program ends.

### What is the accuracy of your part-of-speech tagger? ###
Accuracy: 0.9586210334770795

### What are the precision, recall and F-score for each of the named entity types for your named entity recognizer, and what is the overall F-score? ###

B-LOC 
Precision : 0.8126385809312638 & Recall : 0.7449186991869918 Fscore: 0.77729

B-PER
Precision : 0.9547218628719275 & Recall : 0.6039279869067103 Fscore: 0.739849624

B-MISC 
Precision : 0.8013937282229965 & Recall : 0.5168539325842697 Fscore:  0.62842

B-ORG 
Precision : 0.8694929343308395 & Recall : 0.6156562683931724 Fscore: 0.721

Overall fscore: 0.731

Note: Took help from classmates to calculate the values for each entity.

### What happens if you use your Naive Bayes classifier instead of your perceptron classifier (report performance metrics)? Why do you think that is? ###

When we use Naive Bayes instead of our perceptron we get an accuracy of 91% in POS tagging. A vanilla perceptron with features as bag-of-words also gives accuracy around 90%. So, we can conclude that our averaged perceptron giving an accuracy of >95% performs better because we consider features(like prev POS tag, prev - next words, etc) that depicts relationship between words in a sentence. This extra information/context is responsible for a better performance of our averaged perceptron. Whereas, Naive Bayes considers that each feature is independent.

Accuracy Naive Bayes - 0.9116
Accuracy Averaged Perceptron:0.9586210334770795

Low performance of Naive Bayes in POS tagging and a high performance in SPAM detection also confirms that the task of POS tagging requires us to consider relationship between neighboring words in a sentence.