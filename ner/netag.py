__author__ = 'mohit'
import sys
import os
import json
import io
from perceptron import Perceptron

SENTENCE_START_CONST = 'sent_Start/sent_Start/sent_start'
SENTENCE_END_CONST = 'sent_End/sent_End/sent_End'
TEMP_FILE_CONST = '_temp'
WORD_LABELS = [ 'wprev:', 'wcurr:', 'wnext:']
POS_LABELS = ['pos1:','pos2:']
NER_LABELS = ['ner1:','ner2:']
SUFFIX_LABELS = ['suffix2:','suffix3:']

def getWordFeatures(word):

    suffix_features = ''
    suffix_features += SUFFIX_LABELS[0]+word[len(word)-2:] if len(word) > 1 else SUFFIX_LABELS[0]+'0'
    suffix_features += ' '+SUFFIX_LABELS[1]+word[len(word)-3:] if len(word) > 2 else ' '+SUFFIX_LABELS[1]+'0'
    return suffix_features


def getfeatures(words, index, ner_predictions):

    words_features = ''
    delim = ''
    for w in range(len(WORD_LABELS)):
        i = index+w-1
        if (i >= 0) and (i < len(words)):
            mytemp = words[i]
        elif i < 0:
            mytemp = SENTENCE_START_CONST
        else:
            mytemp = SENTENCE_END_CONST
        words_features += delim + WORD_LABELS[w]+'/'.join(mytemp.split('/')[:-1])
        delim = ' '

    pos_features = ''
    for w in range(len(POS_LABELS)):
        i = index+w-len(POS_LABELS)
        if i < 0:
            mytemp = SENTENCE_START_CONST
        else:
            mytemp = words[i]

        pos_features += delim + POS_LABELS[w]+mytemp.split('/')[-1]

    ner_features = ''
    for w in range(len(NER_LABELS)):
        temp = ner_predictions[w-2] if (len(ner_predictions) - (len(NER_LABELS) - w -1) > 0) \
            else SENTENCE_START_CONST.split('/')[-1]
        ner_features += delim + NER_LABELS[w]+temp

    return words_features+pos_features+ner_features

# def create_pos_features(line):
#     word_dict = {}
#     words = line.split()
#
#     for index in range(len(words)):
#
#         features = getfeatures(words,index)


def main():
    # This is the classifier for POS tagger
    # Takes model_file name as I/P
    # model_file contains Average Weight to be intialized for average Perceptron

    if len(sys.argv) != 2 or sys.argv[1] == '' or not os.path.isfile(os.path.abspath(sys.argv[1])):
        print("Invalid Arguments!")
        exit(1)

    mode_file_name = sys.argv[1]

    try:
        with open(mode_file_name,"r") as mode_file_handle:
            labeltoweight_dict = json.loads(mode_file_handle.read())
    except IOError:
        print("Error: Cannot open File")
        sys.exit(1)
    except TypeError:
        print("Error: Invalid JSON Model File")
        sys.exit(1)

    perceptron = Perceptron()
    input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8', errors='ignore')
    for line in input_stream:

        line.strip()

        tagged_output = ''

        my_delim = ''

        ner_predictions = []
        words = line.split()
        for index in range(len(words)):

            features = getfeatures(words, index, ner_predictions)

            #features += ' ' +getWordFeatures('/'.join(words[index].split('/')[:-1]))

            feature_vector_dict = perceptron.create_feature_vector(features.split())

            tag = perceptron.percep_classify(labeltoweight_dict, feature_vector_dict)
            ner_predictions.append(tag)

            tagged_output += my_delim + words[index] + "/" + tag

            my_delim = ' '

        print(tagged_output)
        sys.stdout.flush()

if __name__ == "__main__":
    main()