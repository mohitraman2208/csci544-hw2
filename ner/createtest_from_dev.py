__author__ = 'mohit'
import sys
import io

# python scriptname output_file < dev_file > test_file
def main():

    output_file = sys.argv[1]
    input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8', errors='ignore')
    with open(output_file, 'w', errors='ignore') as output_hdl:
        for line in input_stream:

            line = line.strip()

            words = line.split()

            newline = ''
            delim = ''
            for word in words:

                word_label = word.split('/')
                output_hdl.write(word_label[-1]+'\n')
                newline += delim + '/'.join(word.split('/')[:-1])
                delim = ' '
            print(newline)

if __name__ == "__main__":
    main()