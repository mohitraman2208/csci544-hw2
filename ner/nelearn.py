__author__ = 'mohit'
import sys
import os.path
import json
from perceptron import Perceptron

SENTENCE_START_CONST = 'sent_Start/sent_Start/sent_start'
SENTENCE_END_CONST = 'sent_End/sent_End/sent_End'
TEMP_FILE_CONST = '_temp'
WORD_LABELS = [ 'wprev:', 'wcurr:', 'wnext:']
POS_LABELS = ['pos1:','pos2:','pos3:']
NER_LABELS = ['ner1:','ner2:']
SUFFIX_LABELS = ['suffix2:','suffix3:']

def getWordFeatures(word):

    suffix_features = ''
    suffix_features += SUFFIX_LABELS[0]+word[len(word)-2:] if len(word) > 1 else SUFFIX_LABELS[0]+'0'
    suffix_features += ' '+SUFFIX_LABELS[1]+word[len(word)-3:] if len(word) > 2 else ' '+SUFFIX_LABELS[1]+'0'
    return suffix_features

def getfeatures(words, index):

    words_features = ''
    delim = ''
    for w in range(len(WORD_LABELS)):
        i = index+w-1
        if (i >= 0) and (i < len(words)):
            mytemp = words[i]
        elif i < 0:
            mytemp = SENTENCE_START_CONST
        else:
            mytemp = SENTENCE_END_CONST
        words_features += delim + WORD_LABELS[w]+'/'.join(mytemp.split('/')[:-2])
        delim = ' '

    pos_features = ''
    for w in range(len(POS_LABELS)):
        i = index+w-len(POS_LABELS)+1
        if i < 0:
            mytemp = SENTENCE_START_CONST
        else:
            mytemp = words[i]

        pos_features += delim + POS_LABELS[w]+mytemp.split('/')[-2]

    ner_features = ''
    for w in range(len(NER_LABELS)):
        i = index+w-len(NER_LABELS)
        if i < 0:
            mytemp = SENTENCE_START_CONST
        else:
            mytemp = words[i]

        ner_features += delim + NER_LABELS[w]+mytemp.split('/')[-1]

    return words_features+pos_features+ner_features



def process_train_file(train_file_name, labels):

    processed_file = train_file_name+TEMP_FILE_CONST
    with open(processed_file, 'w') as output_file:

        with open(os.path.abspath(train_file_name), 'r', errors='ignore') as train_file_handle: # with open(os.path.abspath('tst_input.txt'),'r',errors='ignore') as train_file_handle:

            for line in train_file_handle:

                line = line.strip()

                words = line.split()

                for index in range(len(words)):

                    curr_word_label = words[index].split('/')[-1]

                    if curr_word_label not in labels:
                        labels.append(curr_word_label)

                    newLine = curr_word_label+ ' ' +getfeatures(words, index)

                   # newLine += ' ' +getWordFeatures('/'.join(words[index].split('/')[:-2]))
                    # next_word = NEXT_WORD_LABEL_CONST+words[index+1].split('/')[0] \
                      #  if index < len(words) - 1 else NEXT_WORD_LABEL_CONST+SENTENCE_END_CONST

                    #newLine = curr_word_label[1] + ' ' + prev_word+' '+CURR_WORD_LABEL_CONST+curr_word_label[0]+' '+next_word

                    output_file.write(newLine + '\n')

                    # prev_word = PREV_WORD_LABEL_CONST+curr_word_label[0]

    return processed_file

def main():
    # This is the training program for POS tagger
    # Take two command line arguments - training_file model_file
    # training_file contains training data and the learned model is stored in model file

    if len(sys.argv) != 3 or sys.argv[1] == '' or not os.path.isfile(os.path.abspath(sys.argv[1])) or sys.argv[2] == '':
        print("Invalid Arguments!")
        exit(1)

    # Command line argument 1 - Training file name
    train_file_name = sys.argv[1]

    labels = []

    processed_train_file = process_train_file(train_file_name,labels)

    # processed_file_content = process_train_file(train_file_name)
    # Command line argument 2 - Model file name
    model_file_name = sys.argv[2]

    percepLearn = Perceptron()
    percepLearn.init_percep_learn(processed_train_file, model_file_name, labels, 20)

    model = percepLearn.percep_learn()

    with open(model_file_name,'w') as output_file:
        output_file.write(json.dumps(model,indent=1))

    #os.remove(processed_train_file)

if __name__ == "__main__":
    main()