__author__ = 'mohit'

import linecache
import mmap
import random

# Helper function to count number of lines from a huge file
# I/P - file Handle of open file
# O/P - Number of lines
def map_count(filename):
    f = open(filename, "r+")
    buf = mmap.mmap(f.fileno(), 0)
    lines = 0
    readline = buf.readline
    while readline():
        lines += 1
    f.close()
    return lines


class Perceptron:

    def __init__(self):
        self.label_avg_weights_dict = {}
        self.word_last_iter_count = {}

        self.training_file_name = ""
        self.model_file_name = ""
        self.test_file_name = ""
        self.max_iter = 10
        self.labels = []

    def init_percep_learn(self, training_file_name, model_file_name, labels, max_iter=10):
        self.training_file_name = training_file_name
        self.model_file_name = model_file_name
        self.max_iter = max_iter
        self.labels = set(labels)
        Perceptron.init_label_dict(self.label_avg_weights_dict, self.labels)
        Perceptron.init_label_dict(self.word_last_iter_count, self.labels)

    @staticmethod
    def init_label_dict(label_dict, labels):
        # Initialize dictionary
        for label in labels:
            weight_vector_dict = {}
            label_dict[label] = weight_vector_dict

    @staticmethod
    def create_feature_vector(words_feature):
        feature_vector = {}
        for word in words_feature:
            feature_vector[word] = feature_vector[word] + 1 if word in feature_vector else 1

        return feature_vector

    @staticmethod
    def percep_classify(label_wght_dict, feature_vector):

        # init
        res_label = ""
        res_max = float("-inf")

        for label, weight_vector in label_wght_dict.items():

            sum_weights = 0.0

            for feature, feature_count in feature_vector.items():

                sum_weights = sum_weights + feature_count*weight_vector[feature] if feature in weight_vector else sum_weights

            if sum_weights > res_max:
                res_label = label
                res_max = sum_weights

        return res_label

    # Training function returns weight vector Perceptron
    def percep_learn(self):

        # init
        label_weights_dict = {}
        cache_label_weights_dict = {}
        Perceptron.init_label_dict(label_weights_dict, self.labels)
        Perceptron.init_label_dict(cache_label_weights_dict, self.labels)
        iteration_count = 0

        count_lines = map_count(self.training_file_name)
        line_numbers_final = range(count_lines)

        # For N Iterations -
        for i in range(self.max_iter):

            line_numbers = list(line_numbers_final)

            # Shuffling training Data
            random.shuffle(line_numbers)

            # For each training example
            for line_number in line_numbers:

                word_features = linecache.getline(self.training_file_name, line_number + 1).split()

                actual_label = word_features[0]

                feature_vector = Perceptron.create_feature_vector(word_features[1:])

                predicted_label = Perceptron.percep_classify(label_weights_dict, feature_vector)

                if actual_label != predicted_label:

                    Perceptron.correct_weight_vectors(label_weights_dict, actual_label, predicted_label, feature_vector)
                    Perceptron.addto_cache_weight_vectors(cache_label_weights_dict, actual_label,predicted_label,
                                                          feature_vector, iteration_count)
                iteration_count += 1

        return_dict = Perceptron.calc_label_avg_vector(label_weights_dict,cache_label_weights_dict, iteration_count)
        return return_dict #self.label_avg_weights_dict

    @staticmethod
    def addto_cache_weight_vectors(cache_label_weights_dict, actual_label, predicted_label, feature_vector, iteration_count):

        predict_label_wght_vector = cache_label_weights_dict[predicted_label]
        actual_label_wght_vector = cache_label_weights_dict[actual_label]

        for feature, feature_count in feature_vector.items():
            actual_label_wght_vector[feature] = actual_label_wght_vector[feature] + (iteration_count*feature_count) \
                if feature in actual_label_wght_vector else feature_count*iteration_count
            predict_label_wght_vector[feature] = predict_label_wght_vector[feature] - (feature_count*iteration_count)\
                if feature in predict_label_wght_vector else (-feature_count)*iteration_count

    @staticmethod
    def calc_label_avg_vector(label_weights_dict, cache_label_weights_dict, iteration_count):

        for label, feature_wght_dict in label_weights_dict.items():
            for feature, feature_count in feature_wght_dict.items():
                label_weights_dict[label][feature] -= (cache_label_weights_dict[label][feature]/iteration_count)

        return  label_weights_dict
    # def addto_avg_wght_dict(self, feature_vector, iteration_count):
    #
    #     for label in self.labels:
    #         for word,word_count in feature_vector.items():
    #
    #             self.label_avg_weights_dict[label][word] = (self.label_avg_weights_dict[label][word] + word_count) >> 1 \
    #                 if word in self.label_avg_weights_dict[label] else word_count >> 1
    #             # if word in self.label_avg_weights_dict[label]:
                #
                #     multiplier = iteration_count - self.word_last_iter_count[label][word]
                #
                #     self.label_avg_weights_dict[label][word] = self.label_avg_weights_dict[label][word]*multiplier + word_count
                #
                #     self.word_last_iter_count[label][word] = iteration_count
                # else:
                #
                #     self.word_last_iter_count[label][word] = iteration_count
                #
                #     self.label_avg_weights_dict[label][word] = word_count

        # for label in self.labels:
        #     weight_vector = Counter(label_weights_dict[label])
        #     avg_weight_vector = Counter(self.label_avg_weights_dict[label])
        #
        #     self.label_avg_weights_dict[label] = dict(weight_vector+avg_weight_vector)

            # for word, word_wght in label_weights_dict[label].items():
            #     self.label_avg_weights_dict[label][word] = self.label_avg_weights_dict[label][word] + word_wght \
            #         if word in self.label_avg_weights_dict[label] else word_wght


    @staticmethod
    def correct_weight_vectors(label_dict, actual_label, predicted_label, feature_vector_dict):

        predict_label_wght_vector = label_dict[predicted_label]
        actual_label_wght_vector = label_dict[actual_label]

        for feature, feature_count in feature_vector_dict.items():
            actual_label_wght_vector[feature] = actual_label_wght_vector[feature] + feature_count \
                if feature in actual_label_wght_vector else feature_count
            predict_label_wght_vector[feature] = predict_label_wght_vector[feature] - feature_count\
                if feature in predict_label_wght_vector else -feature_count