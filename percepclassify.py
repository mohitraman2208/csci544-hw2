import sys
import json
import os
from perceptron import Perceptron


def main():

    # This is the training program for percepclassify Perceptron Classifier
    # Take two command line arguments - training_file model_file
    # training_file contains training data and the learned model is stored in model file

    if len(sys.argv) != 2 or sys.argv[1] == '' or not os.path.isfile(os.path.abspath(sys.argv[1])):
        print("Invalid Arguments!")
        exit(1)

    mode_file_name = sys.argv[1]

    # test_file_name = sys.argv[2]

    # Load JSON data from Model file
    try:
        with open(mode_file_name,"r") as mode_file_handle:
            labeltoweight_dict = json.loads(mode_file_handle.read())
    except IOError:
        print("Error: Cannot open File")
        sys.exit(1)
    except TypeError:
        print("Error: Invalid JSON Model File")
        sys.exit(1)

    perceptron = Perceptron()
    try:
        #f_out = open("output.txt","w")

        for line in sys.stdin.readlines():

            features_words = line.split()

            feature_vector_dict = perceptron.create_feature_vector(features_words)

            print(perceptron.percep_classify(labeltoweight_dict, feature_vector_dict))
            sys.stdout.flush()

    except IOError:
        print("Cannot open Test file")


if __name__ == '__main__':
    main()